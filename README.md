# <ins>**PLEASE READ THIS BEFORE DOWNLOADING**</ins>

# Salwyrr Launcher AppImage

### *Unofficial Salwyrr Launcher AppImage to install and run Salwyrr Launcher on Linux.*

## Installation

#### Graphical User Interface

+ [**Download**](https://gitlab.com/luky_sheep/salwyrr-launcher-appimage/-/raw/main/Salwyrr_Launcher-x86_64.AppImage) the AppImage <br>

+ **Right Click** on the File > **Properties** > **Permissions** > Check **"Allow Executing / Running File as Program"** box <br>

+ **Run** the File (Double **Left-click** or **Right Click** > **"Open"**) <br>

#### Terminal

<ins>Download</ins> :

- Using `curl` :
```sh
curl https://gitlab.com/luky_sheep/salwyrr-launcher-appimage/-/raw/main/Salwyrr_Launcher-x86_64.AppImage --output - > Salwyrr_Launcher-x86_64.AppImage
```

- Using `git` :
```
git clone git@gitlab.com:luky_sheep/salwyrr-launcher-appimage.git
cd salwyrr-launcher-appimage
```

<ins>Allow</ins> the AppImage to Run :
```
chmod +x Salwyrr_Launcher-x86_64.AppImage
```

<ins>Run</ins> the AppImage :
```
./Salwyrr_Launcher-x86_64.AppImage
```

## Description

#### The Unofficial [Salwyrr Launcher](https://www.salwyrr.com) AppImage lets you install and launch Salwyrr Launcher, easily and quickly.

**Features** added to improve Salwyrr Launcher Installation :

- Installs [Java 8](https://www.java.com/download) if necessary, or creates a directory linking to an existing Java 8 directory.

- In the menu, a brand new **Salwyrr_Launcher** application will appear in the Game category.

- In the terminal, a `Salwyrr_Launcher` command will be added as well. *(Require ~/.local/bin as a default path)*

## Support

#### Any question / issue ?

- Ask for help on the [Salwyrr Launcher International Discord Server](https://www.discord.gg/salwyrr)
- Send me a DM (Direct Message) on Discord <br>
    **ID** : 564878931929464853 <br>
    **Tag** : Luky_sheep#7462 <br>


## Contribution

Thanks to my fellow Linux mates who took part in the Project :<br>
- TaliAly (Beta-Testing, Help) <br>
- itIsMeDavid (Help) <br>
- Antoine (Help) <br>
- And all the Others Linux users that tested the AppImage <br>

## Screenshots

![Downloading Java](https://cdn.discordapp.com/attachments/766682967279140874/911696685749657620/salwyrr-download.png)
