#!/bin/sh



### CHECKING ###

appImageContentHead="appImageContentHead.txt"
localBin="${HOME}/.local/bin"
salwyrrLocalBin="${localBin}/Salwyrr_Launcher"

arguments="! -readable -prune -o -type f -size 26576064c -executable -print"

find "$localBin" ${arguments} | grep -q "$salwyrrLocalBin" && exit 0
# Stop Script if Salwyrr AppImage already in .local/bin

### CHECKING - END ###



### FINDING ###

logs (){
	lang=$(locale | grep LANG | cut -d= -f2 | cut -d_ -f1)
	if [ "$lang" = "pt" ]; then
	    locationAppImageTx="Localização da Salwyrr Launcher AppImage: $2"
	    warningNotFoundTx="[AVISO] Salwyrr Launcher AppImage não encontrada; Instalação do Salwyrr Launcher incompleta"
	elif [ "$lang" = "fr" ]; then
	    locationAppImageTx="Localisation de l'AppImage Salwyrr Launcher : $2"
	    warningNotFoundTx="[AVERTISSEMENT] Aucun AppImage de Salwyrr Launcher trouvé ; Installation de l'Application Salwyrr Launcher incomplète"
	else
	    locationAppImageTx="Location of Salwyrr Launcher AppImage : $2"
	    warningNotFoundTx="[WARNING] No Salwyrr Launcher AppImage found; Salwyrr Launcher Application Installation incomplete"
	fi

	[ -n "$1" ] && echo "[$(date +%T)] >> ${locationAppImageTx}" && return
	echo "[$(date +%T)] >> ${warningNotFoundTx}" && return
}


SELF=$(readlink -f "$0")
path=${SELF%/*}

filesList=$(find "$HOME" ${arguments})
for file in $filesList; do
    appImageContent="$(cat "${path}"/"${appImageContentHead}")"
    if hexdump -n2016 -C "${file}" | grep -Fxq "${appImageContent}" ; then
	appImageLocation=$(find "$HOME" -wholename "$file")
	logs 1 "$appImageLocation"

	rm -f "$salwyrrLocalBin"
	cp "$appImageLocation" "$salwyrrLocalBin"
	chmod +x "$salwyrrLocalBin"
	exit 0
    fi
done
logs 0

### FINDING - END ###

