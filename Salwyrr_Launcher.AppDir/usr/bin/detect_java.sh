#!/bin/sh


javaVer () {
	$1 -version 2>&1 | awk -F '"' '{print $2}' | cut -d "." -f2 | tr -d '\n'
}


link() {
	rm -rf "$2"
	ln -rs "$1" "$2"
}


javaLocal() {
	[ ! "$localJavaVer" = 8 ] && [ ! -n "$(command -v java)" ] && link "$slwJavaBin" "$localBinJava"
}


lookingTx() {
	lang=$(locale | grep LANG | cut -d= -f2 | cut -d_ -f1)
	tx="Looking for Java 8..."
	[ "$lang" = "pt" ] && tx="Procurando por Java 8..."
	[ "$lang" = "fr" ] && tx="Recherche de Java 8 en cours..."
	echo "[$(date +%T)] >> $tx"
}



slwJavaDirPath=${HOME}"/.Salwyrr-Java"
slwJavaBin=${slwJavaDirPath}"/bin/java"
localBinJava=${HOME}"/.local/bin/java"

localJavaVer=$(javaVer "$localBinJava")
slwJavaVer=$(javaVer "$slwJavaBin")


if [ ! "$slwJavaVer" = 8 ] ; then

	lookingTx

	filesList=$(find /usr/ /opt/ "$HOME"/ -iname "java" -type f -executable)
	for javaFile in $filesList; do
		javaVerInt=$(javaVer "$javaFile")
		if [ -n "$javaVerInt" ] && [ "$javaVerInt" = "8" ]; then
			java8Dir=$(dirname -- "$(dirname -- "$javaFile")" | tr -d '\n')
			link "$java8Dir" "$slwJavaDirPath"
			javaLocal			
			exit 0
	       	fi
	done
	exit 1
else
	javaLocal
fi

# END

exit 0


















